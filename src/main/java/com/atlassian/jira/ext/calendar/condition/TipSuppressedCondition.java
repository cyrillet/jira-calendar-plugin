package com.atlassian.jira.ext.calendar.condition;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.ext.calendar.SuppressedTipsManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.UserPropertyManager;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;

import java.util.Map;
import org.apache.log4j.Logger;


/**
 * A condition that checks if a tip is suppressed for the authenticated user.
 *
 * The tip's key must be provided as a parameter named "tipKey".
 *
 * Note: this class is copied from jira-issue-nav-plugin. We should really move it into jira-api.
 */
public class TipSuppressedCondition implements Condition
{
    private static final Logger log = Logger.getLogger(TipSuppressedCondition.class);

    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final SuppressedTipsManager suppressedTipsManager;
    private String tipKey;

    public TipSuppressedCondition(JiraAuthenticationContext jiraAuthenticationContext, UserPropertyManager userPropertyManager)
    {
        this.jiraAuthenticationContext = jiraAuthenticationContext;

        // TODO: This really should be injected, but conditions are created in a DI container that doesn't contain
        // plugin components. The underlying cause of this problem is fixed in 5.1, so we can remove this hack then.
        this.suppressedTipsManager = new SuppressedTipsManager(userPropertyManager);
    }

    public void init(Map<String, String> params) throws PluginParseException
    {
        tipKey = params.get("tipKey");
    }

    public boolean shouldDisplay(Map<String, Object> context)
    {
        final User user = jiraAuthenticationContext.getLoggedInUser();

        // We can't store preferences for anonymous users, so we never show them dismissible tips
        if (user == null)
        {
            return false;
        }

        try
        {
            return suppressedTipsManager.isSuppressed(tipKey, user);
        }
        catch (IllegalArgumentException e)
        {
            log.warn("Unable to check if the info tip '" + tipKey + "' has been suppressed or not.", e);
            return false;
        }
    }
}