package com.atlassian.jira.ext.calendar.model;

import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.core.util.map.EasyMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.ofbiz.OfBizListIterator;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.util.EasyList;
import org.jmock.Mock;
import org.jmock.MockObjectTestCase;
import org.ofbiz.core.entity.EntityExpr;

import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class TestVersionDelegator extends MockObjectTestCase
{

    private Mock mockOfbizDelegator;
    private Mock mockOfBizListIterator;
    private Mock mockVersion;
    private Mock mockVersionManager;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();


        mockOfBizListIterator = new Mock(OfBizListIterator.class);
        mockVersion = new Mock(Version.class);
        mockVersionManager = new Mock(VersionManager.class);
        mockOfbizDelegator = new Mock(OfBizDelegator.class);
    }

    public void testGetVersionsList()
    {
        mockOfBizListIterator
                .expects(atLeastOnce())
                .method("next")
                .withNoArguments()
                .will(onConsecutiveCalls(
                        returnValue(new MockGenericValue("Project", EasyMap.build("id", new Long(1)))),
                        returnValue(null)
                ));
        mockOfBizListIterator
                .expects(once())
                .method("close")
                .withNoArguments();
        mockVersionManager
                .expects(once())
                .method("getVersion")
                .with(eq(new Long(1)))
                .will(returnValue(mockVersion.proxy()));

        mockOfbizDelegator
                .expects(once())
                .method("findListIteratorByCondition")
                .with(eq("Version"), isA(EntityExpr.class))
                .will(returnValue(mockOfBizListIterator.proxy()));

        VersionDelegator versionDelegator = new VersionDelegator((OfBizDelegator) mockOfbizDelegator.proxy(), (VersionManager) mockVersionManager.proxy());

        assertEquals(
                Collections.EMPTY_LIST,
                versionDelegator.getVersionsList(new Date(), new Date(), Collections.<Project>emptySet()));

        Date start = new Date();
        Date end = new Date();
        Set<Project> projects = new HashSet<Project>();
        Mock mockProject = new Mock(Project.class);

        mockProject.expects(once()).method("getId").withNoArguments().will(returnValue(1L));

        projects.add((Project) mockProject.proxy());

        assertEquals(EasyList.build(mockVersion.proxy()), versionDelegator.getVersionsList(start, end, projects));
        mockVersionManager.verify();
        mockOfBizListIterator.verify();
    }
}
