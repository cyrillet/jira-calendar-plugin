package com.atlassian.jira.ext.calendar.model;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.index.DocumentConstants;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.security.PermissionManager;
import org.jmock.Mock;
import org.jmock.MockObjectTestCase;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

public class TestCalendarSearchRequest extends MockObjectTestCase
{
    private Mock mockPermissionManager;

    private Mock mockSearchRequestService;

    private Mock mockProjectManager;

    private Mock mockSearchProvider;

    private Mock mockSearchService;

    private Mock mockCustomFieldManager;

    private Mock mockProject;

    private Mock mockIssue1;

    private Mock mockIssue2;

    private List versionList;

    private TestVersionDelegator versionDelegator;

    protected void setUp() throws Exception
    {
        super.setUp();

        /* Otherwise, this test will fail horribly. */
        Class.forName("org.hsqldb.jdbcDriver").newInstance();
        
        mockPermissionManager = new Mock(PermissionManager.class);
        mockProjectManager = new Mock(ProjectManager.class);
        mockSearchRequestService = new Mock(SearchRequestService.class);
        mockSearchProvider = new Mock(SearchProvider.class);
        mockSearchService = new Mock(SearchService.class);
        mockCustomFieldManager = new Mock(CustomFieldManager.class);
        mockProject = new Mock(Project.class);
        mockIssue1 = new Mock(Issue.class);
        mockIssue2 = new Mock(Issue.class);

        Mock mockVersion1;
        Mock mockVersion2;

        versionList = new ArrayList();

        mockVersion1 = new Mock(Version.class);
        mockVersion1.expects(atLeastOnce()).method("getId").withNoArguments().will(returnValue(new Long(1)));
        mockVersion1.expects(atLeastOnce()).method("getReleaseDate").withNoArguments().will(returnValue(null));
        versionList.add(mockVersion1.proxy());

        mockVersion2 = new Mock(Version.class);
        mockVersion2.expects(atLeastOnce()).method("getId").withNoArguments().will(returnValue(new Long(2)));
        mockVersion2.expects(atLeastOnce()).method("getReleaseDate").withNoArguments().will(returnValue(null));
        versionList.add(mockVersion2.proxy());

        versionDelegator = new TestVersionDelegator((OfBizDelegator) new Mock(OfBizDelegator.class).proxy());
        versionDelegator.setVersionList(versionList);
    }

//    protected User getUser(final String name)
//    {
//        Mock mockUser = new Mock(User.class);
//
//        mockUser.expects(atLeastOnce()).method("getName").withNoArguments().will(returnValue(name));
//        mockUser.expects(atLeastOnce()).method("getDirectoryId").withNoArguments().will(returnValue(1l));
//        mockUser.expects(atLeastOnce()).method("isActive").withNoArguments().will(returnValue(true));
//        mockUser.expects(atLeastOnce()).method("getEmailAddress").withNoArguments().will(returnValue(name + "@atlassian.com"));
//        mockUser.expects(atLeastOnce()).method("getDisplayName").withNoArguments().will(returnValue(name));
//
//        return (User) mockUser.proxy();
//    }

    protected CalendarSearchRequest getCalendarSearchRequest(
            User user,
            Long searchIdentifier,
            Long projectId,
            Long startOffset,
            Long endOffset,
            VersionDelegator versionDelegator,
            PermissionManager permissionManager,
            ProjectManager projectManager,
            SearchRequestService searchRequestService,
            SearchService searchService,
            CustomFieldManager customFieldManager) throws SearchException, InvalidFilterSearchRequestException, InvalidProjectSearchRequestException
    {
        return new CalendarSearchRequest(
                user, searchIdentifier, projectId, startOffset, endOffset, versionDelegator, permissionManager, projectManager, searchRequestService, searchService, customFieldManager);

    }

    public void testInstantiateWithSearchIdentifierAndOffsets() throws Exception
    {
//        User user = new MockOSUser("admin", "admin", "admin@atlassian.com");
//        List<Issue> issues = Arrays.asList((Issue) mockIssue1.proxy(), (Issue) mockIssue2.proxy());
//
//        mockIssue1.expects(atLeastOnce()).method("getDueDate").withNoArguments().will(returnValue(null));
//        mockIssue2.expects(atLeastOnce()).method("getDueDate").withNoArguments().will(returnValue(new Timestamp(System.currentTimeMillis())));
//        mockPermissionManager.expects(once()).method("getProjectObjects").with(eq(Permissions.BROWSE), same(user)).will(returnValue(Arrays.asList(mockProject.proxy())));
//
//        SearchRequest sr = new SearchRequest();
//        mockSearchRequestService.expects(once()).method("getFilter").with(isA(JiraServiceContext.class), eq(10000L)).will(returnValue(sr));
//
//        SearchResults searchResult = new SearchResults(issues, PagerFilter.getUnlimitedFilter());
//        mockSearchProvider
//                .expects(once())
//                .method("search")
//                .with(isA(Query.class), isA(User.class), isA(PagerFilter.class))
//                .will(returnValue(searchResult));
//
//        CalendarSearchRequest calendarSearchRequest = getCalendarSearchRequest(
//                    user,
//                    10000L,
//                    null,
//                    -DateUtils.DAY_MILLIS,
//                    DateUtils.DAY_MILLIS,
//                    versionDelegator,
//                    (PermissionManager) mockPermissionManager.proxy(),
//                    (ProjectManager) mockProjectManager.proxy(),
//                    (SearchRequestService) mockSearchRequestService.proxy(),
//                    (SearchService) mockSearchService.proxy()
//        );
//
//
//        assertNotNull(calendarSearchRequest);
    }

//    public void testInstantiateWithProjectIdAndOffsets() throws Exception
//    {
//        User user = new MockUser("admin", "admin", "admin@atlassian.com");
//        List<Issue> issues = Arrays.asList((Issue) mockIssue1.proxy(), (Issue) mockIssue2.proxy());
//
//        mockIssue1.expects(atLeastOnce()).method("getDueDate").withNoArguments().will(returnValue(null));
//        mockIssue2.expects(atLeastOnce()).method("getDueDate").withNoArguments().will(returnValue(new Timestamp(System.currentTimeMillis())));
//        mockProject.expects(atLeastOnce()).method("getKey").withNoArguments().will(returnValue("TST"));
//        mockPermissionManager.expects(once()).method("getProjectObjects").with(eq(Permissions.BROWSE), same(user)).will(returnValue(Arrays.asList(mockProject.proxy())));
//        mockProjectManager.expects(once()).method("getProjectObj").with(eq(10000L)).will(returnValue(mockProject.proxy()));
//
//        SearchResults searchResult = new SearchResults(issues, PagerFilter.getUnlimitedFilter());
//        mockSearchProvider
//                .expects(once())
//                .method("search")
//                .with(isA(Query.class), isA(User.class), isA(PagerFilter.class))
//                .will(returnValue(searchResult));
//
//        Mock mockProject = new Mock(Project.class);
//        mockProject.expects(once()).method("getName").withNoArguments().will(returnValue("Test Project"));
//        mockProject.expects(atLeastOnce()).method("getKey").withNoArguments().will(returnValue("TST"));
//        mockProjectManager.expects(once()).method("getProjectObj").with(eq(10000L)).will(returnValue(mockProject.proxy()));
//
//        CalendarSearchRequest calendarSearchRequest = getCalendarSearchRequest(
//                    user,
//                    null,
//                    10000L,
//                    -DateUtils.DAY_MILLIS,
//                    DateUtils.DAY_MILLIS,
//                    versionDelegator,
//                    (PermissionManager) mockPermissionManager.proxy(),
//                    (ProjectManager) mockProjectManager.proxy(),
//                    (SearchRequestService) mockSearchRequestService.proxy(),
//                    (SearchService) mockSearchService.proxy()
//        );
//
//        assertNotNull(calendarSearchRequest);
//    }

//    public void testIsCalendarableVersion() throws Exception
//    {
//        User user = new MockOSUser("admin", "admin", "admin@atlassian.com");
//        List<Issue> issues = Arrays.asList((Issue) mockIssue1.proxy(), (Issue) mockIssue2.proxy());
//
//        mockIssue1.expects(atLeastOnce()).method("getDueDate").withNoArguments().will(returnValue(null));
//        mockIssue2.expects(atLeastOnce()).method("getDueDate").withNoArguments().will(returnValue(new Timestamp(System.currentTimeMillis())));
//        mockPermissionManager.expects(once()).method("getProjectObjects").with(eq(Permissions.BROWSE), same(user)).will(returnValue(Arrays.asList(mockProject.proxy())));
//
//        SearchRequest searchRequest = new SearchRequest();
//        mockSearchRequestService.expects(once()).method("getFilter").with(isA(JiraServiceContext.class), ANYTHING).will(returnValue(searchRequest));
//
//        SearchResults searchResult = new SearchResults(issues, PagerFilter.getUnlimitedFilter());
//        mockSearchProvider
//                .expects(once())
//                .method("search")
//                .with(ANYTHING, same(user), isA(PagerFilter.class))
//                .will(returnValue(searchResult));
//
//        CalendarSearchRequest calendarSearchRequest = getCalendarSearchRequest(
//                user,
//                10000L,
//                null,
//                null,
//                null,
//                versionDelegator,
//                (PermissionManager) mockPermissionManager.proxy(),
//                (ProjectManager) mockProjectManager.proxy(),
//                (SearchRequestService) mockSearchRequestService.proxy(),
//                (SearchService) mockSearchService.proxy()
//        );
//
//        Mock mockVersion;
//        Version version;
//        long currentTimeMillis = System.currentTimeMillis();
//
//        mockVersion = new Mock(Version.class);
//        mockVersion.expects(atLeastOnce()).method("getReleaseDate").withNoArguments().will(returnValue(new Date(currentTimeMillis + Byte.MAX_VALUE)));
//        version = (Version) mockVersion.proxy();
//        assertTrue(calendarSearchRequest.isCalendarableVersion(version));
//
//        mockVersion = new Mock(Version.class);
//        mockVersion.expects(atLeastOnce()).method("getReleaseDate").withNoArguments().will(returnValue(new Date(currentTimeMillis - Byte.MAX_VALUE)));
//        version = (Version) mockVersion.proxy();
//        assertTrue(calendarSearchRequest.isCalendarableVersion(version));
//
//        mockVersion = new Mock(Version.class);
//        mockVersion.expects(atLeastOnce()).method("getReleaseDate").withNoArguments().will(returnValue(new Date(currentTimeMillis + (12 * (7 * DateUtils.DAY_MILLIS) + 1))));
//        version = (Version) mockVersion.proxy();
//        assertFalse(calendarSearchRequest.isCalendarableVersion(version));
//
//        mockVersion = new Mock(Version.class);
//        mockVersion.expects(atLeastOnce()).method("getReleaseDate").withNoArguments().will(returnValue(null));
//        version = (Version) mockVersion.proxy();
//
//        assertFalse(calendarSearchRequest.isCalendarableVersion(version));
//    }

//    public void testSearchRequestForDay() throws Exception {
//
//        User user = new MockOSUser("admin", "admin", "admin@atlassian.com");
//        List<Issue> issues = Arrays.asList((Issue) mockIssue1.proxy(), (Issue) mockIssue2.proxy());
//
//        mockIssue1.expects(atLeastOnce()).method("getDueDate").withNoArguments().will(returnValue(null));
//        mockIssue2.expects(atLeastOnce()).method("getDueDate").withNoArguments().will(returnValue(new Timestamp(System.currentTimeMillis())));
//        mockPermissionManager.expects(once()).method("getProjectObjects").with(eq(Permissions.BROWSE), same(user)).will(returnValue(Arrays.asList(mockProject.proxy())));
//
//        SearchRequest searchRequest = new SearchRequest();
//        mockSearchRequestService.expects(once()).method("getFilter").with(isA(JiraServiceContext.class), eq(10000L)).will(returnValue(searchRequest));
//
//        SearchResults searchResult = new SearchResults(issues, PagerFilter.getUnlimitedFilter());
//        mockSearchProvider
//                .expects(once())
//                .method("search")
//                .with(ANYTHING, isA(User.class), isA(PagerFilter.class))
//                .will(returnValue(searchResult));
//
//        CalendarSearchRequest calendarSearchRequest;
//
//        calendarSearchRequest = getCalendarSearchRequest(
//                user,
//                10000L,
//                null,
//                null,
//                null,
//                versionDelegator,
//                (PermissionManager) mockPermissionManager.proxy(),
//                (ProjectManager) mockProjectManager.proxy(),
//                (SearchRequestService) mockSearchRequestService.proxy(),
//                (SearchService) mockSearchService.proxy()
//        );
//
//        assertFalse(searchRequest.getQuery().toString().equals(calendarSearchRequest.getSearchRequestForDay(new Date()).getQuery().toString()));
//    }

    protected class CalendarSearchRequest extends com.atlassian.jira.ext.calendar.model.CalendarSearchRequest
    {
        public CalendarSearchRequest(User user, Long searchIdentifier, Long projectId, Long startOffset, Long endOffset,
                VersionDelegator versionDelegator, PermissionManager permissionMgr, ProjectManager projectManager, SearchRequestService searchRequestService, SearchService searchService, CustomFieldManager customFieldManager)
                throws InvalidFilterSearchRequestException, InvalidProjectSearchRequestException, SearchException
        {
            super(user, searchIdentifier, projectId, DocumentConstants.ISSUE_DUEDATE, startOffset, endOffset, versionDelegator, permissionMgr, projectManager, searchRequestService, searchService, customFieldManager);
        }

        @Override
        protected SearchProvider getSearchProvider()
        {
            return (SearchProvider) mockSearchProvider.proxy();
        }
    }

    protected class TestVersionDelegator extends VersionDelegator
    {
        private List versionList;

        public TestVersionDelegator(OfBizDelegator ofbizDelegator)
        {
            super(ofbizDelegator, null);
        }

        public List getVersionsList(Date startDate, Date endDate, Set projects)
        {
            return versionList;
        }

        public void setVersionList(List versionList)
        {
            this.versionList = versionList;
        }
    }
}
